terraform {
    backend "s3" {
        bucket = "tf-mauriciogomes"
        region = "us-east-1"
        key    = "route53/tf.state"
    }
}
provider "aws" {
    region = var.region
}